﻿using System;

namespace Preselector
{
    public class StateEventArgs : EventArgs
    {
        public byte GainLO1 { get; set; }
        public byte GainHI1 { get; set; }
        public byte GainPreamp1 { get; set; }
        public NumberPreampFilter StatePreamp1 { get; set; }
        public FreqLO StateFilter1 { get; set; }
        public FlagOnOff Power1 { get; set; }
        public FlagOnOff Power3_3V { get; set; }
        public FlagOnOff Power12V { get; set; }
        public byte GainLO2 { get; set; }
        public byte GainHI2 { get; set; }
        public byte GainPreamp2 { get; set; }
        public NumberPreampFilter StatePreamp2 { get; set; }
        public FreqLO StateFilter2 { get; set; }
        public FlagOnOff Power2 { get; set; }
        public short LaserPower { get; set; }
        public short Temperature { get; set; }

        public StateEventArgs(byte GainLO1, byte GainHI1, byte GainPreamp1, NumberPreampFilter StatePreamp1, FreqLO StateFilter1, FlagOnOff Power1,
            FlagOnOff Power3_3V, FlagOnOff Power12V, byte GainLO2, byte GainHI2, byte GainPreamp2, NumberPreampFilter StatePreamp2, FreqLO StateFilter2, FlagOnOff Power2, short LaserPower, short Temperature)
        {
            this.GainLO1 = GainLO1;
            this.GainHI1 = GainHI1;
            this.GainPreamp1 = GainPreamp1;
            this.StatePreamp1 = StatePreamp1;
            this.StateFilter1 = StateFilter1;
            this.Power1 = Power1;
            this.Power3_3V = Power3_3V;
            this.Power12V = Power12V;
            this.GainLO2 = GainLO2;
            this.GainHI2 = GainHI2;
            this.GainPreamp2 = GainPreamp2;
            this.StatePreamp2 = StatePreamp2;
            this.StateFilter2 = StateFilter2;
            this.Power2 = Power2;
            this.LaserPower = LaserPower;
            this.Temperature = Temperature;
        }

        public StateEventArgs()
        {
            GainLO1 = 0;
            GainHI1 = 0;
            GainPreamp1 = 0;
            StatePreamp1 = 0;
            StateFilter1 = 0;
            Power1 = 0;
            Power3_3V = 0;
            Power12V = 0;
            GainLO2 = 0;
            GainHI2 = 0;
            GainPreamp2 = 0;
            StatePreamp2 = 0;
            StateFilter2 = 0;
            Power2 = 0;
            LaserPower = 0;
            Temperature = 0;
        }
    }

    public class AdditionalStateEventArgs : EventArgs
    {
        public byte Gain { get; set; }
        public OnOffSwitch Preamp { get; set; }
        public ModePreamp Strengthening { get; set; }
        public ModePreamp StateSwitches { get; set; }
        public PreselectorOnOff StatePreamp { get; set; }
        public OnOffSwitch PowerFirstPreselector { get; set; }
        public OnOffSwitch PowerSecondPreselector { get; set; }
        public OnOffSwitch StatePowerLaser { get; set; }
        public byte FirstNumberChannel { get; set; }
        public byte SecondNumberChannel { get; set; }
        public byte AmplifaerLevel { get; set; }

        public AdditionalStateEventArgs()
        {
            Gain = 0;
            Preamp = 0;
            Strengthening = 0;
            StateSwitches = 0;
            StatePreamp = 0;
            StatePowerLaser = 0;
            AmplifaerLevel = 0;
            FirstNumberChannel = 0;
            SecondNumberChannel = 0;
        }

        public AdditionalStateEventArgs(byte Gain, OnOffSwitch Preamp, ModePreamp Strengthening, ModePreamp StateSwitches, PreselectorOnOff StatePreamp, OnOffSwitch PowerFirstPreselector, OnOffSwitch PowerSecondPreselector, OnOffSwitch StatePowerLaser, byte FirstNumberChannel, byte SecondNumberChannel, byte AmplifaerLevel)
        {
            this.Gain = Gain;
            this.Preamp = Preamp;
            this.Strengthening = Strengthening;
            this.StateSwitches = StateSwitches;
            this.PowerFirstPreselector = PowerFirstPreselector;
            this.PowerSecondPreselector = PowerSecondPreselector;
            this.StatePreamp = StatePreamp;
            this.StatePowerLaser = StatePowerLaser;
            this.AmplifaerLevel = AmplifaerLevel;
            this.FirstNumberChannel = FirstNumberChannel;
            this.SecondNumberChannel = SecondNumberChannel;
        }
    }

    public class FreqGainEventArgs : EventArgs
    {
        public short Freq { get; set; }
        public byte Gain { get; set; }

        public FreqGainEventArgs(short Freq, byte Gain)
        {
            this.Freq = Freq;
            this.Gain = Gain;
        }
    }

    public class  GainEventArgs : EventArgs
    {
        public byte Gain_1 { get; set; }
        public byte Gain_2 { get; set; }

        public GainEventArgs(byte Gain_1, byte Gain_2)
        {
            this.Gain_1 = Gain_1;
            this.Gain_2 = Gain_2;
        }
    }

    public class ExternalSwithes : EventArgs
    {
        public byte FirstNumberChannel { get; set; }
        public byte SecondNumberChannel { get; set; }

        public ExternalSwithes()
        {
            FirstNumberChannel = 0;
            SecondNumberChannel = 0;
        }

        public ExternalSwithes(byte FirstNumberChannel, byte SecondNumberChannel)
        {
            this.FirstNumberChannel = FirstNumberChannel;
            this.SecondNumberChannel = SecondNumberChannel;
        }
    }

    public class ConfigurationChannel : EventArgs
    {
        public byte NumberChannel { get; set; }
        public byte FirstSwitchCode { get; set; }
        public byte SecondSwitchCode { get; set; }

        public ConfigurationChannel()
        {
            NumberChannel = 0;
            FirstSwitchCode = 0;
            SecondSwitchCode = 0;
        }

        public ConfigurationChannel(byte NumberChannel, byte FirstSwitchCode, byte SecondSwitchCode)
        {
            this.NumberChannel = NumberChannel;
            this.FirstSwitchCode = FirstSwitchCode;
            this.SecondSwitchCode = SecondSwitchCode;
        }
    }

    public class CurrentStatusOfTuners : EventArgs
    {
        public StatusOfTuner FirstTuner { get; set; }
        public StatusOfTuner SecondTuner { get; set; }

        public CurrentStatusOfTuners()
        {
            this.FirstTuner = new StatusOfTuner(0,0,0,0);
            this.SecondTuner = new StatusOfTuner(0, 0, 0, 0);
        }

        public CurrentStatusOfTuners(byte FirstLockDetect2, byte FirstLockDetect1, byte FirstBusy, short FirstTemperature, byte SecondLockDetect2, byte SecondLockDetect1, byte SecondBusy, short SecondTemperature)
        {
            this.FirstTuner = new StatusOfTuner(FirstLockDetect2, FirstLockDetect1, FirstBusy, FirstTemperature); 
            this.SecondTuner = new StatusOfTuner(SecondLockDetect2, SecondLockDetect1, SecondBusy, SecondTemperature);
        }
    }

    public class StatusOfTuner : EventArgs
    {
        public byte LockDetect2 { get; set; }
        public byte LockDetect1 { get; set; }
        public byte Busy { get; set; }
        public short Temperature { get; set; }

        public StatusOfTuner()
        {
            LockDetect2 = 0;
            LockDetect1 = 0;
            Busy = 0;
            Temperature = 0;
        }

        public StatusOfTuner(byte LockDetect2, byte LockDetect1, byte Busy, short Temperature)
        {
            this.LockDetect2 = LockDetect2;
            this.LockDetect1 = LockDetect1;
            this.Busy = Busy;
            this.Temperature = Temperature;
        }
    }

    public class SwitchingCodesExternalSwithes : EventArgs
    {
        public SwitchingCodes FirstSwith { get; set; }
        public SwitchingCodes SecondSwith { get; set; }

        public SwitchingCodesExternalSwithes()
        {
            FirstSwith = new SwitchingCodes();
            SecondSwith = new SwitchingCodes();
        }

        public SwitchingCodesExternalSwithes(SwitchingCodes FirstSwith, SwitchingCodes SecondSwith)
        {
            this.FirstSwith = FirstSwith;
            this.SecondSwith = SecondSwith;
        }
    }

    public class SwitchingCodes
    {
        public byte Off { get; set; }
        public byte Ch1 { get; set; }
        public byte Ch2 { get; set; }
        public byte Ch3 { get; set; }
        public byte Ch4 { get; set; }
        public byte Ch5 { get; set; }
        public byte Ch6 { get; set; }
        public byte Ch7 { get; set; }
        public byte Ch8 { get; set; }
        public byte Ch9 { get; set; }
        public byte Ch10 { get; set; }
        public byte Ch11 { get; set; }
        public byte Ch12 { get; set; }

        public SwitchingCodes()
        {
            this.Off = 0;
            this.Ch1 = 0;
            this.Ch2 = 0;
            this.Ch3 = 0;
            this.Ch4 = 0;
            this.Ch5 = 0;
            this.Ch6 = 0;
            this.Ch7 = 0;
            this.Ch8 = 0;
            this.Ch9 = 0;
            this.Ch10 = 0;
            this.Ch11 = 0;
            this.Ch12 = 0;
        }

        public SwitchingCodes(byte Off, byte Ch1, byte Ch2, byte Ch3, byte Ch4, byte Ch5, byte Ch6, byte Ch7, byte Ch8, byte Ch9, byte Ch10, byte Ch11, byte Ch12)
        {
            this.Off = Off;
            this.Ch1 = Ch1;
            this.Ch2 = Ch2;
            this.Ch3 = Ch3;
            this.Ch4 = Ch4;
            this.Ch5 = Ch5;
            this.Ch6 = Ch6;
            this.Ch7 = Ch7;
            this.Ch8 = Ch8;
            this.Ch9 = Ch9;
            this.Ch10 = Ch10;
            this.Ch11 = Ch11;
            this.Ch12 = Ch12;
        }
    }
}
