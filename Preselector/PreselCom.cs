﻿using System;
using System.IO.Ports;
using System.Linq;

namespace Preselector
{
    /// <summary>
    /// Presel Com type connection.
    /// </summary>
    public class PreselCOM : PreselModel
    {
        /// <summary>
        /// The serial port.
        /// </summary>
        private SerialPort serialPort;

        /// <summary>
        /// Message for create new array.
        /// </summary>
        private string flagMessage = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreselCOM"/> class.
        /// </summary>
        /// <param name="portName">
        /// Port name.
        /// </param>
        /// <param name="baudRate">
        /// Baud rate.
        /// </param>
        public PreselCOM(string portName, int baudRate)
        {
            this.serialPort = new SerialPort();
            this.PortName = portName;
            this.BaudRate = baudRate;
        }

        /// <summary>
        /// Event on Read byte.
        /// </summary>
        public override event EventHandler<string> OnRead;

        /// <summary>
        /// Event on write byte.
        /// </summary>
        public override event EventHandler<string> OnWrite;
        
        /// <summary>
        /// Gets or sets the port name.
        /// </summary>
        private string PortName { get; set; }

        /// <summary>
        /// Gets or sets the baud rate.
        /// </summary>
        private int BaudRate { get; set; }
        
        /// <summary>
        /// The connect.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool Connect()
        {
            try
            {
                this.serialPort.PortName = this.PortName;
                this.serialPort.BaudRate = this.BaudRate;
                this.serialPort.Open();
                this.serialPort.DataReceived += new SerialDataReceivedEventHandler(this.p_DataReceivedHandler);
                return this.serialPort.IsOpen;
            }
            catch { return serialPort.IsOpen; }
        }

        /// <summary>
        /// Disconnect Com.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool Disconnect()
        {
            if (this.serialPort.IsOpen)
            {
                this.serialPort.Close();
            }
            return this.serialPort.IsOpen;
        }

        /// <summary>
        /// Send message by Com.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        protected override void Send(string message)
        {
            var messageMass = message.Split(' ');
            message = string.Join(string.Empty, messageMass);
            var buffer = new byte[messageMass.Length];
            buffer = Enumerable.Range(0, message.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(message.Substring(x, 2), 16))
                .ToArray();
            try
            {
                this.flagMessage = string.Empty;
                if (this.serialPort.IsOpen)
                {
                    this.serialPort.Write(buffer, 0, buffer.Length);
                    this.OnWrite?.Invoke(this, message);
                    this.flagMessage = message;
                }
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Receive by Com.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void p_DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            if (e.EventType == SerialData.Eof)
                return;
            try
            {
                var message = string.Empty;
                var newMessage = string.Empty;
                var buffer = new byte[this.serialPort.BytesToRead];
                this.serialPort.Read(buffer, 0, this.flagMessage.Length / 2);
                message = PreselModel.ConvertToString(buffer);
                var secondByte = SecondByte(message);
                newMessage += $"{message}";
                this.DecryptionAnswer(newMessage);
                this.OnRead?.Invoke(this, newMessage);
            }
            catch
            {
                this.Disconnect();
            }
        }
        
    }
}
