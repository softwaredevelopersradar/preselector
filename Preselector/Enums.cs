﻿namespace Preselector
{
    public enum PreselectorNumber
    {
        AllPreselector,
        FirstPreselector,
        SecondPreselector
    }

    public enum FeedChannels
    {
        OffBothChannel,
        OnFirstChannel,
        OnSecondChannel,
        OnBothChannel
    }

    public enum FreqLO
    {
        Range30_70MHz = 15,
        Range60_90MHz = 13,
        Range75_131MHz = 11,
        Range120_210MHz = 9,
        Range186_340MHz = 7,
        Range292_490MHz = 5,
        Range470_880MHz = 3,
        Range860_1500MHz = 1,
        Range1500_3000MHzOrWiFi2_4GHz = 40,
        Range3000_4000MHz = 16,
        Range4000_5000MHz = 20,
        Range5000_6000MHz = 30
    }

    public enum NumberPreampFilter
    {
        Range500_1500MHzAndUHF = 16,
        Range1500_3000MHzAndUHF = 37,
        More3000MHzAndUHF = 65,
        Less500MHzAndUHF = 14,
        Bypass = 2,
        Blocking = 3,
        WiFi2_4GHzAndUHF = 132
    }

    public enum FlagOnOff
    {
        OFF,
        ON
    }

    public enum PreselectorOnOff
    {
        OFF,
        ON,
        Blocking
    }

    public enum ExternalSwitchState
    {
        CH1 = 1,
        CH2 = 2,
        CH3 = 3,
        CH4 = 4,
        OFF = 5
    }

    public enum ModePreamp
    {
        AllRange,
        CurrentRange
    }
    
    public enum OnOffPreamp
    {
        ON,
        OFF
    }

    public enum SetPreampRange
    {
        Range30_70MHz,
        Range60_900MHz,
        Range75_131MHz,
        Range120_210MHz,
        Range186_340MHz,
        Range292_490MHz,
        Range470_880MHz,
        Range860_1500MHz,
        Range1500_3000MHz,
        Range3000_4000MHz,
        Range4000_5000MHz,
        Range5000_6000MHz,
        WiFi2_4GHz
    }

    public enum RFChannel
    {
        AllRFChannelsOFF,
        FirstChannel,
        SecondChannel,
        ThirdChannel,
        FourthChannel
    }

    public enum OnOffSwitch
    {
        OFF,
        ON
    }

    public enum BaudRate
    {
        Baud110 = 110,
        Baud300 = 300,
        Baud600 = 600,
        Baud1200 = 1200,
        Baud2400 = 2400,
        Baud4800 = 4800,
        Baud9600 = 9600,
        Baud19200 = 19200,
        Baud38400 = 38400,
        Baud57600 = 57600,
        Baud115200 = 115200,
        Baud230400 = 230400
    }

    public enum Mode
    {
        FourByte,
        SixByte
    }
}
