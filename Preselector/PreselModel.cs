﻿using System;
using System.Linq;

namespace Preselector
{
    /// <summary>
    /// Base class for connect to preselector.
    /// </summary>
    public abstract class PreselModel
    {
        #region Event`s

        public abstract event EventHandler<string> OnRead;

        public abstract event EventHandler<string> OnWrite;

        public event EventHandler<PreselectorNumber> OnPreselectorNumber;

        public event EventHandler<StateEventArgs> OnState_00;

        public event EventHandler<short> OnFreq_01;

        public event EventHandler<byte> OnGain_02_3b;

        public event EventHandler<GainEventArgs> OnGain_02_4b;

        public event EventHandler<ModePreamp> OnModePreamp_03;

        public event EventHandler<PreselectorOnOff> OnOnOffPreamp_04;

        public event EventHandler<SetPreampRange> OnSetPreampRange_05;

        public event EventHandler<FreqGainEventArgs> OnSetFreqGain_06;

        public event EventHandler<ModePreamp> OnModeExternalSwitches_07;

        public event EventHandler<ExternalSwithes> OnManagementExternalSwitches_08;

        public event EventHandler<byte> OnGain_09_3b;

        public event EventHandler<GainEventArgs> OnGain_09_4b;

        public event EventHandler<byte> OnAttenuatorLevelLow_0A;

        public event EventHandler<byte> OnAttenuatorLevelHigh_0B;

        public event EventHandler<FeedChannels> OnFeedChannel_0E;

        public event EventHandler<OnOffSwitch> OnOnOffSwitch_0F;

        public event EventHandler<OnOffSwitch> OnOnOffOpticalTransmitter_10;

        public event EventHandler<ModePreamp> OnModeAttenuator_11;

        public event EventHandler<ConfigurationChannel> OnConfigurationChanels_12;

        public event EventHandler<bool> OnSave_13;

        public event EventHandler<bool> OnReset_14;

        public event EventHandler<byte> OnGain_15;

        public event EventHandler<AdditionalStateEventArgs> OnAdditionalState_16;

        public event EventHandler<OnOffSwitch> OnFeedLaser_17;

        public event EventHandler<byte> OnGainRFAttenuator_18;

        public event EventHandler<StatusOfTuner> OnCurrentStatusOfTuners_19_4b;

        public event EventHandler<CurrentStatusOfTuners> OnCurrentStatusOfTuners_19_6b;

        public event EventHandler<bool> OnReset_1A;

        public event EventHandler<SwitchingCodesExternalSwithes> OnSwitchingCodes_1B;
        #endregion

        /// <summary>
        /// Open COM\TCP type connection
        /// </summary>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract bool Connect();

        /// <summary>
        /// Disconnect COM\TCP
        /// </summary>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract bool Disconnect();
        
        /// <summary>
        /// Query the current state of the preselector
        /// </summary>
        public void GetStatePreselectorCode0()
        {
            this.Send("00000000000000000000000000000000");
        }

        /// <summary>
        /// Frequency setting
        /// </summary>
        /// <param name="freqMHz">Freq MHz</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetFreqCode1(short freqMHz, PreselectorNumber deviceAddress)
        {
            var lowByteFreq = string.Empty;
            var highByteFreq = string.Empty;
            if (freqMHz < 30)
            {
                freqMHz = 30;
            }
            else if (freqMHz > 6000)
            {
                freqMHz = 6000;
            }

            var hex = freqMHz.ToString("X");
            if (hex.Length != 0)
            {
                switch (hex.Length)
                {
                    case 2:
                        lowByteFreq = hex;
                        highByteFreq = "00";
                        break;
                    case 3:
                        {
                            var str = hex.Substring(1, 2);
                            lowByteFreq = str;
                            str = hex.Substring(0, 1);
                            highByteFreq = $"0{str}";
                            break;
                        }

                    case 4:
                        {
                            var str = hex.Substring(1, 2);
                            lowByteFreq = str;
                            str = hex.Substring(0, 2);
                            highByteFreq = str;
                            break;
                        }
                }
            }

            this.Send($"0{(byte)deviceAddress}01{lowByteFreq}{highByteFreq}");
        }

        /// <summary>
        /// Gain setting
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetGainCode2_3b(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }
            
            this.Send($"0{(byte)deviceAddress}02{gain}");
        }

        /// <summary>
        /// Gain setting
        /// </summary>
        /// <param name="valueGainDb031">First gain 0..31 dB</param>
        /// <param name="valueGain2Db031">Second gain 0..31 dB</param>
        public void SetGainCode2_4b(byte valueGainDb031, byte valueGain2Db031)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            if (valueGain2Db031 > 31)
            {
                valueGain2Db031 = 31;
            }

            var gain2 = valueGain2Db031.ToString("X");
            if (gain2.Length < 2)
            {
                gain2 = "0" + gain2;
            }
            
            this.Send($"0002{gain}{gain2}");
        }

        /// <summary>
        /// Setting the preamplifier operating mode - for all ranges or for the current range
        /// </summary>
        /// <param name="paramPreamp">Mode preamp</param>
        public void SetPreampCode3(ModePreamp paramPreamp)
        {
            this.Send($"00030{(byte)paramPreamp}");
        }

        /// <summary>
        /// Preamp on / off
        /// </summary>
        /// <param name="onOffValue">Preselector ON/OFF</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetOnOffCode4(PreselectorOnOff onOffValue, PreselectorNumber deviceAddress)
        {
            this.Send($"0{(byte)deviceAddress}040{(byte)onOffValue}");
        }

        /// <summary>
        /// Setting the preselector by range number
        /// </summary>
        /// <param name="preselector">Preselector Range</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetPreselectorSettingCode5(SetPreampRange preselector, PreselectorNumber deviceAddress)
        {
            var hex = ((byte)preselector).ToString("X");
            this.Send($"0{(byte)deviceAddress}050{hex}");
        }

        /// <summary>
        /// Setting the frequency and gain at this frequency
        /// </summary>
        /// <param name="freqMHz">Freq MHz</param>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        public void SetFreqGainCode6(short freqMHz, byte valueGainDb031)
        {
            var lowByteFreq = string.Empty;
            var highByteFreq = string.Empty;
            if (freqMHz < 30)
            {
                freqMHz = 30;
            }
            else if (freqMHz > 6000)
            {
                freqMHz = 6000;
            }

            var hex = freqMHz.ToString("X");
            if (hex.Length != 0)
            {
                switch (hex.Length)
                {
                    case 2:
                        lowByteFreq = hex;
                        highByteFreq = "00";
                        break;
                    case 3:
                    {
                        var str = hex.Substring(1, 2);
                        lowByteFreq = str;
                        str = hex.Substring(0, 1);
                        highByteFreq = $"0{str}";
                        break;
                    }
                    case 4:
                    {
                        var str = hex.Substring(1, 2);
                        lowByteFreq = str;
                        str = hex.Substring(0, 2);
                        highByteFreq = str;
                        break;
                    }
                }
            }

            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }
            
            this.Send($"0006{lowByteFreq}{highByteFreq}{gain}");
        }

        /// <summary>
        /// Setting the operating mode of external switches - for all ranges or for the current range
        /// </summary>
        /// <param name="operationMod">Mode preamp</param>
        public void SetOperationModCode7(ModePreamp operationMod)
        {
            this.Send($"00070{(byte)operationMod}");
        }

        /// <summary>
        /// Management of external PE42xxx switches
        /// </summary>
        /// <param name="firstChannelNumber">Channel number of the first switch 0..f</param>
        /// /// <param name="secondChannelNumber">Channel number of the second switch 0..f</param>
        public void SetLinkCode8(byte firstChannelNumber, byte secondChannelNumber)
        {
            this.Send($"0008{firstChannelNumber.ToString("X")}{secondChannelNumber.ToString("X")}");
        }

        /// <summary>
        /// Setting preamplifier attenuator level, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetAttenuatorLevelCode9_3b(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }
            
            this.Send($"0{(byte)deviceAddress}09{gain}");
        }

        /// <summary>
        /// Setting preamplifier attenuator level, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="valueGain2Db031">Gain 0..31 dB</param>
        public void SetAttenuatorLevelCode9_4b(byte valueGainDb031, byte valueGain2Db031)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }

            if (valueGain2Db031 > 31)
            {
                valueGain2Db031 = 31;
            }

            var gain2 = valueGain2Db031.ToString("X");
            if (gain2.Length < 2)
            {
                gain2 = "0" + gain2;
            }
            
            this.Send($"0009{gain}{gain2}");
        }

        /// <summary>
        /// Setting attenuator level in the lower range, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetAttenuatorLowLevelCodeA(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }
            
            this.Send($"0{(byte)deviceAddress}0A{gain}");
        }

        /// <summary>
        /// Setting attenuator level in the upper range, in dB
        /// </summary>
        /// <param name="valueGainDb031">Gain 0..31 dB</param>
        /// <param name="deviceAddress">Device address</param>
        public void SetAttenuatorHighLevelCodeB(byte valueGainDb031, PreselectorNumber deviceAddress)
        {
            if (valueGainDb031 > 31)
            {
                valueGainDb031 = 31;
            }

            var gain = valueGainDb031.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }
            
            this.Send($"0{(byte)deviceAddress}0B{gain}");
        }

        /// <summary>
        /// Feed Channel
        /// </summary>
        /// <param name="channel">Channel</param>
        public void FeedChannelCodeE(FeedChannels channel)
        {
            this.Send($"000E0{(byte)channel}");
        }

        /// <summary>
        /// Setting 3.3V power supply for external switches
        /// </summary>
        /// <param name="power">Power</param>
        public void SetPowerCodeF(OnOffSwitch power)
        {
            this.Send($"000F0{(byte)power}");
        }

        /// <summary>
        /// Setting 12V supply for optical transmitter or 5V supply for optional RF amplifier or 3.7V supply for AM9017 tuners
        /// </summary>
        /// <param name="power">Power</param>
        public void SetOpticalPowerCode10(OnOffSwitch power)
        {
            this.Send($"00100{(byte)power}");
        }

        /// <summary>
        /// Setting the mode of the attenuator in the preamplifier channel - for all ranges or for the current range
        /// </summary>
        /// <param name="range">Range</param>
        public void SetGainOperationModeCode11(ModePreamp range)
        {
            this.Send($"00110{(byte)range}");
        }

        /// <summary>
        /// Setting channel configuration for the switching system on the PE42xxx switches
        /// </summary>
        /// <param name="switchingChannel">
        /// Switching channel number (0x00 - for the "off" state, 0x01..0x0F for channels 1..15).
        /// </param>
        /// <param name="lowTetrad">
        /// Code for first switch.
        /// </param>
        /// <param name="highTetrad">
        /// Code for second switch.
        /// </param>
        public void SetConfiguringChannelsCode12(
            byte switchingChannel,
            byte lowTetrad,
            byte highTetrad)
        {
            this.Send($"00120{switchingChannel}{lowTetrad}{highTetrad}");
        }

        /// <summary>
        /// Saving the parameters of the current lane
        /// </summary>
        public void SetSaveCode13()
        {
            this.Send("0013");
        }
        
        /// <summary>
        /// Reset preselector
        /// </summary>
        public void ResetCode14()
        {
            this.Send("0014");
        }

        /// <summary>
        /// Setting the nominal gain of the path
        /// </summary>
        /// <param name="valueGainDb0255">Gain 0..255</param>
        public void SetPathGainCode15(byte valueGainDb0255)
        {
            if (valueGainDb0255 > 255)
            {
                valueGainDb0255 = 255;
            }

            var gain = valueGainDb0255.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }
            
            this.Send($"0015{gain}");
        }

        /// <summary>
        /// Additional settings
        /// </summary>
        public void AdditionalSettingsCode16()
        {
            this.Send("001600000000");
        }

        /// <summary>
        /// Setting Power management of AM9017 tuners (only for two at the same time), when the tuners are turned on, they are reset and set to the current frequency
        /// </summary>
        /// <param name="power">Power On\Off</param>
        public void SetLaserPowerCode17(OnOffSwitch power)
        {
            this.Send($"00170{(byte)power}");
        }

        /// <summary>
        /// RF attenuator control for AM9017 tuners.
        /// </summary>
        /// <param name="valueGainDb022">
        /// Gain 0..22.
        /// </param>
        /// <param name="deviceAddress">
        /// Device address.
        /// </param>
        public void RFAttenuatorControlCode18(byte valueGainDb022, PreselectorNumber deviceAddress)
        {
            if (valueGainDb022 > 22)
            {
                valueGainDb022 = 22;
            }

            var gain = valueGainDb022.ToString("X");
            if (gain.Length < 2)
            {
                gain = "0" + gain;
            }
            
            this.Send($"0{(byte)deviceAddress}18{gain}");
        }

        /// <summary>
        /// Query the current state of a specific receiver.
        /// </summary>
        /// <param name="bb">
        /// The bb.
        /// </param>
        /// <param name="cc">
        /// The cc.
        /// </param>
        /// <param name="deviceAddress">
        /// Device address.
        /// </param>
        public void QueryCurrentStateCode19_4b(byte bb, byte cc, PreselectorNumber deviceAddress)
        {
            this.Send($"0{(byte)deviceAddress}18{bb}{cc}");
        }

        /// <summary>
        /// Query the current state of two receivers.
        /// </summary>
        public void QueryCurrentStateCode19()
        {
            this.Send("001900000000");
        }

        /// <summary>
        /// Performs a factory reset with the subsequent tuning of the AM9017 tuners to the current frequency.
        /// </summary>
        public void ResetSetupCode1A(PreselectorNumber deviceAddress)
        {
            this.Send($"0{(byte)deviceAddress}1A");
        }

        /// <summary>
        /// Requesting Patch Codes for External Switches.
        /// </summary>
        public void RequestingPatchCodesCode1B()
        {
            this.Send("001B00000000000000000000000000");
        }

        /// <summary>
        /// Send some command
        /// </summary>
        /// <param name="message"></param>
        public void SetCommand(string message)
        {
            if (message != string.Empty)
            {
                this.Send(message);
            }
        }

        /// <summary>
        /// Convert byte array to correct string.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="buffer">
        /// Byte array.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected static string ConvertToString(byte[] buffer)
        {
            if (buffer.Length == 0)
            {
                return null;
            }

            var message = BitConverter.ToString(buffer).Replace("-", string.Empty);
            message = string.Join(string.Empty, message.ToCharArray().Select((c, i) => i % 2 == 0 ? c.ToString() : $"{c} "));
            return message;
        }

        /// <summary>
        /// Get second byte.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected static string SecondByte(string message)
        {
            var charMes = message.ToCharArray();
            var secondByte = $"{charMes[2]}{charMes[3]}";
            return secondByte;
        }

        /// <summary>
        /// Used by commands, implemented separately for each type of connection
        /// </summary>
        /// <param name="message">Command for Send</param>
        protected abstract void Send(string message);

        /// <summary>
        /// Decryption answer.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        protected void DecryptionAnswer(string message)
        {
            var str = message.Substring(0, 2);
            var value = Convert.ToInt16(str);
            var preselectorNumber = (PreselectorNumber)value;
            this.OnPreselectorNumber?.Invoke(this, preselectorNumber);
            str = message.Substring(3, 2);
            if (message.Length >= 34)
            {
                this.AllDecryption(message, str);
            }
            else if (str != "00")
            {
                this.AllDecryption(message, str);
            }
        }

        /// <summary>
        /// The set additional.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="additional">
        /// The additional.
        /// </param>
        private static void SetAdditional(string state, AdditionalStateEventArgs additional)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            var bite = binary.Substring(7, 1);
            int valueInt = Convert.ToInt16(bite, 2);
            additional.Preamp = (OnOffSwitch)valueInt;

            bite = binary.Substring(6, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.Strengthening = (ModePreamp)valueInt;

            bite = binary.Substring(5, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.StateSwitches = (ModePreamp)valueInt;

            bite = binary.Substring(3, 2);
            valueInt = Convert.ToInt16(bite, 2);
            additional.StatePreamp = (PreselectorOnOff)valueInt;
            
            bite = binary.Substring(2, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.PowerFirstPreselector = (OnOffSwitch)valueInt;

            bite = binary.Substring(1, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.PowerSecondPreselector = (OnOffSwitch)valueInt;

            bite = binary.Substring(0, 1);
            valueInt = Convert.ToInt16(bite, 2);
            additional.StatePowerLaser = (OnOffSwitch)valueInt;
        }

        /// <summary>
        /// Set numbers channel.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="additional">
        /// The additional.
        /// </param>
        private static void SetNumbersChannel(string state, AdditionalStateEventArgs additional)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            var bite = binary.Substring(4, 4);
            int valueInt = Convert.ToInt16(bite, 2);
            additional.FirstNumberChannel = (byte)valueInt;

            bite = binary.Substring(0, 4);
            valueInt = Convert.ToInt16(bite, 2);
            additional.SecondNumberChannel = (byte)valueInt;
        }

        /// <summary>
        /// The set preamp.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="State">
        /// The state.
        /// </param>
        private static void SetPreamp(string state, StateEventArgs State)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            var bite = binary.Substring(0, 1);
            int valueInt = Convert.ToInt16(bite, 2);
            State.Power1 = (FlagOnOff)valueInt;

            bite = binary.Substring(1, 7);
            valueInt = Convert.ToInt16(bite, 2);
            State.StateFilter1 = (FreqLO)valueInt;
        }

        /// <summary>
        /// The set preamp 2.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="State">
        /// The state.
        /// </param>
        private static void SetPreamp2(string state, StateEventArgs State)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            var bite = binary.Substring(0, 1);
            int valueInt = Convert.ToInt16(bite, 2);
            State.Power2 = (FlagOnOff)valueInt;

            bite = binary.Substring(1, 7);
            valueInt = Convert.ToInt16(bite, 2);
            State.StateFilter2 = (FreqLO)valueInt;
        }

        /// <summary>
        /// The switch state.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="State">
        /// The state.
        /// </param>
        private static void SwitchState(string state, StateEventArgs State)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            
            var bite = binary.Substring(1, 1);
            var valueInt = Convert.ToInt16(bite, 2);
            State.Power3_3V = (FlagOnOff)valueInt;

            bite = binary.Substring(0, 1);
            valueInt = Convert.ToInt16(bite, 2);
            State.Power12V = (FlagOnOff)valueInt;
        }

        /// <summary>
        /// Decode switch state_6 b.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <returns>
        /// The <see cref="StatusOfTuner"/>.
        /// </returns>
        private static StatusOfTuner DecodeSwitchState_6b(string state)
        {
            var binary = string.Join(
                string.Empty,
                state.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            var status = new StatusOfTuner();

            var bite = binary.Substring(7, 1);
            var valueInt = Convert.ToInt16(bite, 2);
            status.LockDetect2 = (byte)valueInt;

            bite = binary.Substring(6, 1);
            valueInt = Convert.ToInt16(bite, 2);
            status.LockDetect1 = (byte)valueInt;

            bite = binary.Substring(5, 1);
            valueInt = Convert.ToInt16(bite, 2);
            status.Busy = (byte)valueInt;

            return status;
        }

        /// <summary>
        /// All decryption.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="str">
        /// The str.
        /// </param>
        private void AllDecryption(string message, string str)
        {
            if (str == null)
            {
                return;
            }

            string value;
            str = message.Substring(3, 2);
            switch (str)
            {
                case "00" when message.Length >= 40:
                    {
                        var state = new StateEventArgs();
                        
                        state.GainLO1 = (byte)ValueConverter(message, 6, 2);
                        
                        state.GainHI1 = (byte)ValueConverter(message, 9, 2);
                        
                        state.GainPreamp1 = (byte)ValueConverter(message, 12, 2);
                        
                        state.StatePreamp1 = (NumberPreampFilter)ValueConverter(message, 15, 2);

                        var stateMessage = message.Substring(18, 2);
                        SetPreamp(stateMessage, state);

                        stateMessage = message.Substring(21, 2);
                        SwitchState(stateMessage, state);
                        
                        state.GainLO2 = (byte)ValueConverter(message, 24, 2);
                        
                        state.GainHI2 = (byte)ValueConverter(message, 27, 2);
                        
                        state.GainPreamp2 = (byte)ValueConverter(message, 30, 2);
                        
                        state.StatePreamp2 = (NumberPreampFilter)ValueConverter(message, 33, 2);

                        stateMessage = message.Substring(36, 2);
                        SetPreamp2(stateMessage, state);
                        
                        state.LaserPower = BitConverter.ToInt16(new[] { (byte)ValueConverter(message, 39, 2), (byte)ValueConverter(message, 42, 2) }, 0);
                        
                        state.Temperature = (short)ValueConverter(message, 45, 2);

                        this.OnState_00?.Invoke(this, state);
                        break;
                    }

                case "16":
                    {
                        var additional = new AdditionalStateEventArgs();
                        var state = message.Substring(6, 2);
                        var decValue = Convert.ToInt32(state, 16);
                        additional.Gain = (byte)decValue;

                        state = message.Substring(9, 2);
                        SetAdditional(state, additional);

                        state = message.Substring(12, 2);
                        decValue = Convert.ToInt32(state, 16);
                        additional.AmplifaerLevel = (byte)decValue;

                        state = message.Substring(15, 2);
                        SetNumbersChannel(state, additional);

                        this.OnAdditionalState_16?.Invoke(this, additional);
                        break;
                    }

                case "01":
                    {
                        if (message.Length > 10)
                        {
                            this.OnFreq_01?.Invoke(this, BitConverter.ToInt16(new[] { (byte)ValueConverter(message, 6, 2), (byte)ValueConverter(message, 9, 2) }, 0));
                        }
                        
                        break;
                    }

                case "02" when message.Length > 7 && message.Length < 10:
                    {
                        this.OnGain_02_3b?.Invoke(this, (byte)ValueConverter(message, 6, 2));

                        break;
                    }

                case "02":
                    {
                        if (message.Length > 10)
                        {
                            this.OnGain_02_4b?.Invoke(this, new GainEventArgs((byte)ValueConverter(message, 6, 2), (byte)ValueConverter(message, 9, 2)));
                        }

                        break;
                    }

                case "03":
                    {
                        if (message.Length > 7)
                        {
                            this.OnModePreamp_03?.Invoke(this, (ModePreamp)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "04":
                    {
                        if (message.Length > 7)
                        {
                            this.OnOnOffPreamp_04?.Invoke(this, (PreselectorOnOff)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "05":
                    {
                        if (message.Length > 7)
                        {
                            this.OnSetPreampRange_05?.Invoke(this, (SetPreampRange)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "06":
                    {
                        if (message.Length > 13)
                        {
                            this.OnSetFreqGain_06?.Invoke(this, new FreqGainEventArgs(BitConverter.ToInt16(new[] { (byte)ValueConverter(message, 6, 2), (byte)ValueConverter(message, 9, 2) }, 0), (byte)ValueConverter(message, 12, 2)));
                        }

                        break;
                    }

                case "07":
                    {
                        if (message.Length > 7)
                        {
                            this.OnModeExternalSwitches_07?.Invoke(this, (ModePreamp)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "08":
                    {
                        if (message.Length > 7)
                        {
                            var externalSwitch = new ExternalSwithes();
                            
                            externalSwitch.SecondNumberChannel = (byte)ValueConverter(message, 6, 1);
                            
                            externalSwitch.FirstNumberChannel = (byte)ValueConverter(message, 7, 1);

                            this.OnManagementExternalSwitches_08?.Invoke(this, externalSwitch);
                        }

                        break;
                    }

                case "09" when message.Length > 7 && message.Length < 10:
                    {
                        this.OnGain_09_3b?.Invoke(this, (byte)ValueConverter(message, 6, 2));

                        break;
                    }

                case "09":
                    {
                        if (message.Length > 10)
                        {
                            this.OnGain_09_4b?.Invoke(this, new GainEventArgs((byte)ValueConverter(message, 6, 2), (byte)ValueConverter(message, 9, 2)));
                        }

                        break;
                    }

                case "0A":
                    {
                        if (message.Length > 7)
                        {
                            this.OnAttenuatorLevelLow_0A?.Invoke(this, (byte)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "0B":
                    {
                        if (message.Length > 7)
                        {
                            this.OnAttenuatorLevelHigh_0B?.Invoke(this, (byte)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "0E":
                    {
                        if (message.Length > 7)
                        {
                            this.OnFeedChannel_0E?.Invoke(this, (FeedChannels)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "0F":
                    {
                        if (message.Length > 7)
                        {
                            this.OnOnOffSwitch_0F?.Invoke(this, (OnOffSwitch)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "10":
                    {
                        if (message.Length > 7)
                        {
                            this.OnOnOffOpticalTransmitter_10?.Invoke(this, (OnOffSwitch)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "12":
                    {
                        if (message.Length > 15)
                        {
                            var configuration = new ConfigurationChannel();
                            
                            configuration.NumberChannel = (byte)ValueConverter(message, 6, 2);
                            
                            configuration.SecondSwitchCode = (byte)ValueConverter(message, 9, 1);
                            
                            configuration.FirstSwitchCode = (byte)ValueConverter(message, 10, 1);
                            
                            this.OnConfigurationChanels_12?.Invoke(this, configuration);
                        }

                        break;
                    }

                case "11":
                    {
                        if (message.Length > 7)
                        {
                            this.OnModeAttenuator_11?.Invoke(this, (ModePreamp)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "13":
                    {
                        this.OnSave_13?.Invoke(this, true);
                        break;
                    }

                case "14":
                    {
                        this.OnReset_14?.Invoke(this, true);
                        break;
                    }

                case "15":
                    {
                        if (message.Length > 7)
                        {
                            this.OnGain_15?.Invoke(this, (byte)ValueConverter(message, 6, 2));
                        }

                        break;
                    }
                    
                case "17":
                    {
                        if (message.Length > 7)
                        {
                            this.OnFeedLaser_17?.Invoke(this, (OnOffSwitch)ValueConverter(message, 6, 2));
                        }

                        break;
                    }

                case "18":
                    {
                        this.OnGainRFAttenuator_18?.Invoke(this, (byte)ValueConverter(message, 6, 2));

                        break;
                    }

                case "19" when message.Length > 17:
                    {
                        var status = new CurrentStatusOfTuners();

                        value = message.Substring(6, 2);
                        status.FirstTuner = DecodeSwitchState_6b(value);
                        
                        status.FirstTuner.Temperature = (short)ValueConverter(message, 9, 2);

                        value = message.Substring(12, 2);
                        status.SecondTuner = DecodeSwitchState_6b(value);
                        
                        status.SecondTuner.Temperature = (short)ValueConverter(message, 15, 2);

                        this.OnCurrentStatusOfTuners_19_6b?.Invoke(this, status);

                        break;
                    }

                case "19" when message.Length > 11:
                    {
                        var status = new StatusOfTuner();

                        value = message.Substring(6, 2);
                        status = DecodeSwitchState_6b(value);
                        
                        status.Temperature = (short)ValueConverter(message, 9, 2);

                        this.OnCurrentStatusOfTuners_19_4b?.Invoke(this, status);

                        break;
                    }

                case "1A":
                    {
                        this.OnReset_1A?.Invoke(this, true);
                        break;
                    }

                case "1B":
                    {
                        var codes = new SwitchingCodesExternalSwithes();

                        codes.FirstSwith.Off = (byte)ValueConverter(message, 6, 1);
                        codes.SecondSwith.Off = (byte)ValueConverter(message, 7, 1);

                        codes.FirstSwith.Ch1 = (byte)ValueConverter(message, 9, 1);
                        codes.SecondSwith.Ch1 = (byte)ValueConverter(message, 10, 1);

                        codes.FirstSwith.Ch2 = (byte)ValueConverter(message, 12, 1);
                        codes.SecondSwith.Ch2 = (byte)ValueConverter(message, 13, 1);

                        codes.FirstSwith.Ch3 = (byte)ValueConverter(message, 15, 1);
                        codes.SecondSwith.Ch3 = (byte)ValueConverter(message, 16, 1);

                        codes.FirstSwith.Ch4 = (byte)ValueConverter(message, 18, 1);
                        codes.SecondSwith.Ch4 = (byte)ValueConverter(message, 19, 1);

                        codes.FirstSwith.Ch5 = (byte)ValueConverter(message, 21, 1);
                        codes.SecondSwith.Ch5 = (byte)ValueConverter(message, 22, 1);

                        codes.FirstSwith.Ch6 = (byte)ValueConverter(message, 24, 1);
                        codes.SecondSwith.Ch6 = (byte)ValueConverter(message, 25, 1);

                        codes.FirstSwith.Ch7 = (byte)ValueConverter(message, 27, 1);
                        codes.SecondSwith.Ch7 = (byte)ValueConverter(message, 28, 1);

                        codes.FirstSwith.Ch8 = (byte)ValueConverter(message, 30, 1);
                        codes.SecondSwith.Ch8 = (byte)ValueConverter(message, 31, 1);

                        codes.FirstSwith.Ch9 = (byte)ValueConverter(message, 33, 1);
                        codes.SecondSwith.Ch9 = (byte)ValueConverter(message, 34, 1);

                        codes.FirstSwith.Ch10 = (byte)ValueConverter(message, 36, 1);
                        codes.SecondSwith.Ch10 = (byte)ValueConverter(message, 37, 1);

                        codes.FirstSwith.Ch11 = (byte)ValueConverter(message, 39, 1);
                        codes.SecondSwith.Ch11 = (byte)ValueConverter(message, 40, 1);

                        codes.FirstSwith.Ch12 = (byte)ValueConverter(message, 42, 1);
                        codes.SecondSwith.Ch12 = (byte)ValueConverter(message, 43, 1);
                        
                        this.OnSwitchingCodes_1B?.Invoke(this, codes);
                        break;
                    }
            }
        }

        private static int ValueConverter(string message, int startIndex, int length)
        {
            var value = message.Substring(startIndex, length);
            return Convert.ToInt16(value, 16);
        }
        
    }
}
