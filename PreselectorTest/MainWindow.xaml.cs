﻿using System;
using System.IO;
using System.IO.Ports;
using Microsoft.Win32;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Preselector;

namespace PreselectorTest
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private PreselModel p;
        
        private PreselCOM preselCom;

        private PreselTcp preselTcp;

        private bool status = false;
        private bool typeConnection = false;
        private string LowByteFreq = ""; private string HighByteFreq = ""; private string sendFreq = ""; private string command = "";

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {

            string message = "";
            message = SendBox.Text;
            try
            {
                p.SetCommand(message);
                SendBox.Clear();
            }
            catch (Exception)
            {
                SendBox.Clear();
            }
        }

        bool flag = false;


        private void AnswerDecryption(string message)
        {
            if (flag)
            {
                Display.AppendText("Расшифровка:\r\n" + message + "\r\n");
                flag = false;
                Display.ScrollToEnd();
            }
        }



        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            string temp;
            string temp1 = (string)PortNameBox.SelectedItem;
            temp = (string)BaudRateBox.SelectedItem;
            int temp2 = Convert.ToInt32(temp);
            this.preselCom = new PreselCOM(temp1, temp2);
            this.p = this.preselCom as PreselModel;
            try
            {
                status = false;
                status = p.Connect();
                if (status == true)
                {
                    CheckEl.Fill = new SolidColorBrush(Colors.PaleGreen);
                    this.preselCom.OnWrite += WriteMethod;
                    this.preselCom.OnRead += ReadMethod;
                    p.OnPreselectorNumber += P_OnPreselectorNumber;
                    p.OnState_00 += P_OnState;
                    p.OnFreq_01 += P_OnFreq01;
                    p.OnGain_02_3b += P_OnGain02_3b;
                    p.OnGain_02_4b += P_OnGain02_4b;
                    p.OnModePreamp_03 += P_OnModePreamp;
                    p.OnOnOffPreamp_04 += P_OnOnOffPreamp;
                    p.OnSetPreampRange_05 += P_OnSetPreampRange;
                    p.OnSetFreqGain_06 += P_OnSetFreqGain_06;
                    p.OnModeExternalSwitches_07 += P_OnModeExternalSwitches;
                    p.OnManagementExternalSwitches_08 += P_OnRFChannel;
                    p.OnGain_09_3b += P_OnGain09_3b;
                    p.OnGain_09_4b += P_OnGain09_4b;
                    p.OnAttenuatorLevelLow_0A += P_OnAttenuatorLevelLow;
                    p.OnAttenuatorLevelHigh_0B += P_OnAttenuatorLevelHigh;
                    p.OnFeedChannel_0E += P_OnFeedChannel;
                    p.OnOnOffSwitch_0F += P_OnOnOffSwitch;
                    p.OnOnOffOpticalTransmitter_10 += P_OnOnOffOpticalTransmitter;
                    p.OnModeAttenuator_11 += P_OnModeAttenuator;
                    p.OnConfigurationChanels_12 += P_OnConfigurationChanels;
                    p.OnSave_13 += P_OnSave;
                    p.OnReset_14 += P_OnReset;
                    p.OnGain_15 += P_OnGain15;
                    p.OnAdditionalState_16 += P_OnAdditionalState;
                    p.OnFeedLaser_17 += P_OnFeedLaser;
                    this.p.OnGainRFAttenuator_18 += P_OnGainRFAttenuator_18;
                    this.p.OnCurrentStatusOfTuners_19_4b += P_OnCurrentStatusOfTuners_19_4b;
                    this.p.OnCurrentStatusOfTuners_19_6b += P_OnCurrentStatusOfTuners_19_6b;
                    this.p.OnReset_1A += P_OnReset_1A;
                    this.p.OnSwitchingCodes_1B += P_OnSwitchingCodes_1B;
                }
                else
                {
                    throw new Exception("Port isn`t open. \r\n");
                }
                ConnectButton.IsEnabled = false;
                DisconnectButton.IsEnabled = true;
                SendButton.IsEnabled = true;
                PortNameBox.IsEnabled = false;
                BaudRateBox.IsEnabled = false;
            }
            catch
            {
                throw new Exception("Port isn`t open. \r\n");
            }
        }

        private void P_OnSwitchingCodes_1B(object sender, SwitchingCodesExternalSwithes e)
        {
            Dispatcher.Invoke(() =>
                {
                    Display.AppendText($"Cостояния 1-го - 2-го коммутаторов:\r\n");
                    Display.AppendText($"Off: {e.FirstSwith.Off} - {e.SecondSwith.Off}\r\n");
                    Display.AppendText($"Ch1: {e.FirstSwith.Ch1} - {e.SecondSwith.Ch1}\r\n");
                    Display.AppendText($"Ch2: {e.FirstSwith.Ch2} - {e.SecondSwith.Ch2}\r\n");
                    Display.AppendText($"Ch3: {e.FirstSwith.Ch3} - {e.SecondSwith.Ch3}\r\n");
                    Display.AppendText($"Ch4: {e.FirstSwith.Ch4} - {e.SecondSwith.Ch4}\r\n");
                    Display.AppendText($"Ch5: {e.FirstSwith.Ch5} - {e.SecondSwith.Ch5}\r\n");
                    Display.AppendText($"Ch6: {e.FirstSwith.Ch6} - {e.SecondSwith.Ch6}\r\n");
                    Display.AppendText($"Ch7: {e.FirstSwith.Ch7} - {e.SecondSwith.Ch7}\r\n");
                    Display.AppendText($"Ch8: {e.FirstSwith.Ch8} - {e.SecondSwith.Ch8}\r\n");
                    Display.AppendText($"Ch9: {e.FirstSwith.Ch9} - {e.SecondSwith.Ch9}\r\n");
                    Display.AppendText($"Ch10: {e.FirstSwith.Ch10} - {e.SecondSwith.Ch10}\r\n");
                    Display.AppendText($"Ch11: {e.FirstSwith.Ch11} - {e.SecondSwith.Ch11}\r\n");
                    Display.AppendText($"Ch12:{e.FirstSwith.Ch12} - {e.SecondSwith.Ch12}\r\n");
                });
        }

        private void P_OnReset_1A(object sender, bool e)
        {
            Dispatcher.Invoke(() =>
                {
                    Display.AppendText($"Сброс настроек на заводские с последующей настройкой тюнеров AM9017 на текущую частоту \r\n");
                });
        }

        private void P_OnCurrentStatusOfTuners_19_6b(object sender, CurrentStatusOfTuners e)
        {
            Dispatcher.Invoke(() =>
                {
                    Display.AppendText($"Cостояние тюнера 1:\r\n");
                    Display.AppendText($"{e.FirstTuner.LockDetect2} - lock detect2\r\n");
                    Display.AppendText($"{e.FirstTuner.LockDetect1} - lock detect1\r\n");
                    Display.AppendText($"{e.FirstTuner.Busy} - busy\r\n");
                    Display.AppendText($"{e.FirstTuner.Temperature} - Температура\r\n");
                    Display.AppendText($"Cостояние тюнера 2:\r\n");
                    Display.AppendText($"{e.FirstTuner.LockDetect2} - lock detect2\r\n");
                    Display.AppendText($"{e.FirstTuner.LockDetect1} - lock detect1\r\n");
                    Display.AppendText($"{e.FirstTuner.Busy} - busy\r\n");
                    Display.AppendText($"{e.FirstTuner.Temperature} - Температура\r\n");
                });
        }

        private void P_OnCurrentStatusOfTuners_19_4b(object sender, StatusOfTuner e)
        {
            Dispatcher.Invoke(() =>
                {
                    Display.AppendText($"Cостояние тюнера:\r\n");
                    Display.AppendText($"{e.LockDetect2} - lock detect2\r\n");
                    Display.AppendText($"{e.LockDetect1} - lock detect1\r\n");
                    Display.AppendText($"{e.Busy} - busy\r\n");
                    Display.AppendText($"{e.Temperature} - Температура\r\n");
                });
        }

        private void P_OnGainRFAttenuator_18(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
                {
                    Display.AppendText($"Значение усиления : {e} дБ\r\n");
                });
        }

        private void P_OnSetFreqGain_06(object sender, FreqGainEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка частоты: {e.Freq} МГц\r\n");
                Display.AppendText($"Установка усиления: {e.Gain} дБ\r\n");
            });
        }

        private void P_OnFeedLaser(object sender, OnOffSwitch e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Управление питанием тюнеров: {e}\r\n");
            });
        }

        private void P_OnAdditionalState(object sender, AdditionalStateEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Номинальное усиление преселектора: {e.Gain} дБ\r\n");
                Display.AppendText($"Включение/выключение предусилителя: {e.Preamp}\r\n");
                Display.AppendText($"Управление усилением: {e.Strengthening}\r\n");
                Display.AppendText($"Cостояние внешних коммутаторов: {e.StateSwitches}\r\n");
                Display.AppendText($"Cостояние предусилителя: {e.StatePreamp}\r\n");
                Display.AppendText($"Питание для 1-го канала преселектора: {e.PowerFirstPreselector}\r\n");
                Display.AppendText($"Питание для 2-го канала преселектора: {e.PowerSecondPreselector}\r\n");
                Display.AppendText($"Cостояние питания лазера передатчика: {e.StatePowerLaser}\r\n");
                Display.AppendText($"Уровень усиления для всех диапазонов: {e.AmplifaerLevel} дБ\r\n");
                Display.AppendText($"Номер канала для внешнего коммутатора №1: {e.FirstNumberChannel} дБ\r\n");
                Display.AppendText($"Номер канала для внешнего коммутатора №2: {e.SecondNumberChannel} дБ\r\n");
            });
        }

        private void P_OnGain15(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка номинального усиления тракта: {e} дБ\r\n");
            });
        }

        private void P_OnReset(object sender, bool e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Сброс настроек на заводские\r\n");
            });
        }

        private void P_OnSave(object sender, bool e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Сохранение параметров текущей полосы\r\n");
            });
        }

        private void P_OnConfigurationChanels(object sender, ConfigurationChannel e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Номер канала коммутации: {e.NumberChannel}\r\n");
                Display.AppendText($"Код для 1-го коммутатора: {e.FirstSwitchCode}\r\n");
                Display.AppendText($"Код для 2-го коммутатора: {e.SecondSwitchCode}\r\n");
            });
        }

        private void P_OnModeAttenuator(object sender, ModePreamp e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка режима работы аттенюатора в канале предусилителя: {e}\r\n");
            });
        }

        private void P_OnOnOffOpticalTransmitter(object sender, OnOffSwitch e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Питание 12В для оптического передатчика или питание 5В для дополнительного РЧ-усилителя: {e}\r\n");
            });
        }

        private void P_OnOnOffSwitch(object sender, OnOffSwitch e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Питание 3.3В для внешних коммутаторов ZSWA4-63DRB+: {e}\r\n");
            });
        }

        private void P_OnFeedChannel(object sender, FeedChannels e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Питание каналов преселектора: {e}\r\n");
            });
        }

        private void P_OnAttenuatorLevelHigh(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Уровень аттенюатора в верхнем диапазоне (1500..6000 MHz): {e} дБ\r\n");
            });
        }

        private void P_OnAttenuatorLevelLow(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Уровень аттенюатора в нижнем диапазоне (30..1500MHz): {e} дБ\r\n");
            });
        }

        private void P_OnGain09_3b(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Уровень аттенюатора предусилителя для 2-ух каналов: {e} дБ\r\n");
            });
        }

        private void P_OnGain09_4b(object sender, GainEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Уровень аттенюатора предусилителя для 1-го канала: {e.Gain_1} дБ\r\n");
                Display.AppendText($"Уровень аттенюатора предусилителя для 2-го канала: {e.Gain_2} дБ\r\n");
            });
        }

        private void P_OnRFChannel(object sender, ExternalSwithes e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"1-ый коммутатор включен на: {e.FirstNumberChannel} канал\r\n");
                Display.AppendText($"2-ой коммутатор включен на: {e.SecondNumberChannel} канал\r\n");
            });
        }

        private void P_OnModeExternalSwitches(object sender, ModePreamp e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка режима работы внешних коммутаторов: {e}\r\n");
            });
        }

        private void P_OnSetPreampRange(object sender, SetPreampRange e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Настройка преселектора по номеру диапазона: {e}\r\n");
            });
        }

        private void P_OnOnOffPreamp(object sender, PreselectorOnOff e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Включение/выключение/блокировка предусилителя: {e}\r\n");
            });
        }

        private void P_OnModePreamp(object sender, ModePreamp e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Режима работы предусилителя: {e}\r\n");
            });
        }

        private void P_OnGain02_4b(object sender, GainEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установлено усиление для 1-го канала: {e.Gain_1} дБ\r\n");
                Display.AppendText($"Установлено усиление для 2-го канала: {e.Gain_2} дБ\r\n");
            });
        }

        private void P_OnGain02_3b(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установлено усиление для 2-ух каналов: {e} дБ\r\n");
            });
        }

        private void P_OnFreq01(object sender, short e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установлена частота: {e} МГц\r\n");
            });
        }

        private void P_OnPreselectorNumber(object sender, PreselectorNumber e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Aдрес устройства, к которому идет обращение: {e}\r\n");
            });
        }

        private void P_OnState(object sender, StateEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Значение аттенюатора LO 1-го канала: {e.GainLO1} дБ\r\n");
                Display.AppendText($"Значение аттенюатора HI 1-го канала: {e.GainHI1} дБ\r\n");
                Display.AppendText($"Значение аттенюатора предусилителя 1-го канала: {e.GainPreamp1} дБ\r\n");
                Display.AppendText($"Cостояние предусилителя 1-го канала: {e.StatePreamp1}\r\n");
                Display.AppendText($"Cостояние фильтров преселектора 1-го канала: {e.StateFilter1}\r\n");
                Display.AppendText($"Питание 1-го канала преселектора: {e.Power1}\r\n");
                Display.AppendText($"Питание 3.3В (для коммутаторов 1 и 2): {e.Power3_3V}\r\n");
                Display.AppendText($"Питание 12В для оптического передатчика(или 5В для внешнего РЧ -усилителя): { e.Power12V}\r\n");
                Display.AppendText($"Значение аттенюатора LO 2-го канала: {e.GainLO2} дБ\r\n");
                Display.AppendText($"Значение аттенюатора HI 2-го канала: {e.GainHI2} дБ\r\n");
                Display.AppendText($"Значение аттенюатора предусилителя 2-го канала: {e.GainPreamp2} дБ\r\n");
                Display.AppendText($"Cостояние предусилителя 2-го канала: {e.StatePreamp2}\r\n");
                Display.AppendText($"Cостояние фильтров преселектора 2-го канала: {e.StateFilter2}\r\n");
                Display.AppendText($"Питание 2-го канала преселектора: {e.Power2}\r\n");
                Display.AppendText($"Мощность лазера: {e.LaserPower} мВ\r\n");
                Display.AppendText($"Температура модуля: {e.Temperature} гр. Цельсия\r\n");
                Display.ScrollToEnd();
            });
        }


        private void WriteMethod(object sender, string e)
        {
            e = AddSpaces(e);
            command = e;
            Display.AppendText($"Команда: {e}\r\n");
        }

        private void ReadMethod(object sender, string e)
        {
            Dispatcher.Invoke(() => { Display.AppendText($"Ответ: {e}\r\n"); });
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            status = p.Disconnect();
            this.preselCom.OnWrite -= WriteMethod;

            if (status == false)
            {
                CheckEl.Fill = new SolidColorBrush(Colors.Red);
                ConnectButton.IsEnabled = true;
                DisconnectButton.IsEnabled = false;
                SendButton.IsEnabled = false;
                PortNameBox.IsEnabled = true;
                BaudRateBox.IsEnabled = true;
            }
        }


        private void Test_COM_Ports_Loaded(object sender, RoutedEventArgs e)
        {
            SetPortName("COM");
            SetPortParity(Parity.Even);
            SetPortBaudRate(9600);
            SetPortStopBits(StopBits.None);
            BaudRateget();
            AddToCombo(); AddAddressDevice(); AddGain(); AddPreamp(); AddOnOffPreamp(); AddPreselectorSetting(); AddOperationMode(); AddLink();
            CheckEl.Fill = new SolidColorBrush(Colors.Red);
            ConnectButton.IsEnabled = true;
            DisconnectButton.IsEnabled = false;
            SendButton.IsEnabled = false;
            FreqBox.MaxLength = 4;
            LowAndHighByte.MaxLength = 4;
        }

        public string SetPortName(string defaultPortName)
        {
            string portname = "";
            foreach (string PortName in SerialPort.GetPortNames())
            {
                PortNameBox.Items.Add(PortName);
            }
            PortNameBox.SelectedIndex = 0;
            if (portname == "")
            {
                portname = defaultPortName;
            }
            return portname;
        }

        public int SetPortBaudRate(int defaultPortBaudRate)
        {
            string baudRate = "";
            if (baudRate == "")
            {
                baudRate = defaultPortBaudRate.ToString();
            }
            return int.Parse(baudRate);
        }

        public Parity SetPortParity(Parity defaultPortParity)
        {
            string parity = "";
            foreach (string paritybit in Enum.GetNames(typeof(Parity)))
            {
                ParityBox.Items.Add(paritybit);
            }
            ParityBox.SelectedIndex = 0;
            if (parity == "")
            {
                parity = defaultPortParity.ToString();
            }
            return (Parity)Enum.Parse(typeof(Parity), parity);
        }

        public StopBits SetPortStopBits(StopBits defaultPortStopBits)
        {
            string stopBits = "";
            foreach (string stopbits in Enum.GetNames(typeof(StopBits)))
            {
                stopBits = stopbits;
                StopBitsBox.Items.Add(stopbits);
            }
            StopBitsBox.SelectedIndex = 1;
            if (stopBits == "")
            {
                stopBits = defaultPortStopBits.ToString();
            }
            return (StopBits)Enum.Parse(typeof(StopBits), stopBits);
        }

        public void BaudRateget()
        {
            string[] listbaudrate = new string[15];
            listbaudrate[0] = "110";
            listbaudrate[1] = "300";
            listbaudrate[2] = "600";
            listbaudrate[3] = "1200";
            listbaudrate[4] = "2400";
            listbaudrate[5] = "4800";
            listbaudrate[6] = "9600";
            listbaudrate[7] = "14400";
            listbaudrate[8] = "19200";
            listbaudrate[9] = "38400";
            listbaudrate[10] = "56000";
            listbaudrate[11] = "57600";
            listbaudrate[12] = "115200";
            listbaudrate[13] = "128000";
            listbaudrate[14] = "256000";
            foreach (string baudRate in listbaudrate)
            {
                BaudRateBox.Items.Add(baudRate);
            }
            BaudRateBox.SelectedIndex = 12;

            TypeConnectionComboBox.Items.Add("TCP");
            TypeConnectionComboBox.Items.Add("Com");
            TypeConnectionComboBox.SelectedIndex = 0;
        }

        private void SendBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            string text = tb.Text.ToUpper();
            char[] symbols = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', ' ' };
            tb.Text = new string(text.Where(x => symbols.Contains(x)).ToArray());
            SendBox.SelectionStart = SendBox.Text.Length;
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            Display.Clear();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Text file(*.txt)|*.txt|c# file (*.cs)|*.cs";
            saveFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (saveFile.ShowDialog() == true)
            {
                File.WriteAllText(saveFile.FileName, Display.Text);
            }
        }

        private void AddAddressDevice()
        {
            foreach (string Device in AddDeviceAddressCombo())
            {
                DeviceAddressBox.Items.Add(Device);
            }
            DeviceAddressBox.SelectedIndex = 0;
        }

        private void AddPreselectorSetting()
        {
            foreach (string Preselector in AddRengFreqCombo())
            {
                PreselectorSetting.Items.Add(Preselector);
            }
            PreselectorSetting.SelectedIndex = 0;
        }


        private void AddGain()
        {
            foreach (string Gain in AddGainCombo())
            {
                GainBox.Items.Add(Gain);
                GainBox_2.Items.Add(Gain);
                GainBox1.Items.Add(Gain);
                GainBox2.Items.Add(Gain);
                GainBox2_2.Items.Add(Gain);
                GainBox3.Items.Add(Gain);
                GainBox4.Items.Add(Gain);
                GainBox2_3.Items.Add(Gain);
                GainBox2_1.Items.Add(Gain);
            }
            GainBox_Mode.Items.Add("4 байта");
            GainBox_Mode.Items.Add("6 байт");
            GainBox_2.SelectedIndex = 0;
            GainBox2_2.SelectedIndex = 0;
            GainBox_Mode.SelectedIndex = 0;
            GainBox.SelectedIndex = 0;
            GainBox1.SelectedIndex = 0;
            GainBox2.SelectedIndex = 0;
            GainBox3.SelectedIndex = 0;
            GainBox4.SelectedIndex = 0;
            GainBox2_3.SelectedIndex = 0;
            GainBox2_1.SelectedIndex = 0;
        }

        private void AddPreamp()
        {
            foreach (string Preamp in AddPreampCombo())
            {
                PreampBox.Items.Add(Preamp);
            }
            PreampBox.SelectedIndex = 0;
        }

        private void AddOnOffPreamp()
        {
            foreach (string Preamp in OnOffPreampCombo())
            {
                OnOffPreamp1.Items.Add(Preamp);
                OnOffPreamp2.Items.Add(Preamp);
            }
            foreach (string Preamp in OnOffPreampCombo1())
            {
                OnOffPreamp.Items.Add(Preamp);
            }
            OnOffPreamp.Items.Add("Блокировать");
            OnOffPreamp.SelectedIndex = 0;
            OnOffPreamp1.SelectedIndex = 0;
            OnOffPreamp2.SelectedIndex = 0;
        }

        private void AddOperationMode()
        {
            foreach (string Operation in AddOperationModCombo())
            {
                RangeBox.Items.Add(Operation);
                RangeBox1.Items.Add(Operation);
            }
            RangeBox.SelectedIndex = 0;
            RangeBox1.SelectedIndex = 0;
        }

        private void AddLink()
        {
            //foreach (string Link in AddLinkCombo())
            //{
            //    LinkBox.Items.Add(Link);
            //}
            //LinkBox.SelectedIndex = 0;
        }

        private void SendStatus_Click(object sender, RoutedEventArgs e)
        {
            p.GetStatePreselectorCode0();
        }

        private void SendFreq_Click(object sender, RoutedEventArgs e)
        {
            string message = ""; string hex = "";
            message = FreqBox.Text;
            int value = Convert.ToInt32(message);
            if (value < 30) { message = "30"; }
            else if (value > 6000) { message = "6000"; }
            value = Convert.ToInt32(message);
            hex = value.ToString("X");
            sendFreq = message;
            if (hex.Length != 0)
            {
                if (hex.Length == 2) { LowByteFreq = hex; HighByteFreq = "00"; }
                else if (hex.Length == 3)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 1);
                    HighByteFreq = $"0{str}";
                }
                else if (hex.Length == 4)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 2);
                    HighByteFreq = str;
                }
            }
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetFreqCode1((short)value, (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void SendGain_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            int count_1 = Convert.ToInt32(GainBox_2.SelectedIndex);
            string[] mas_1 = AddValueGainCombo();
            string gain_1 = mas_1[count_1];
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetGainCode2_4b((byte)Convert.ToInt16(gain, 16), (byte)Convert.ToInt16(gain_1, 16));
        }

        private void PreampSend_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(PreampBox.SelectedIndex);
            string[] mas = AddParamPreampCombo();
            string ParamPreamp = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPreampCode3((ModePreamp)((byte)Convert.ToInt16(ParamPreamp)));
        }

        private void OnOffSend_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(OnOffPreamp.SelectedIndex);
            string[] mas = AddParamPreampCombo1();
            string ParamPreamp = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetOnOffCode4((PreselectorOnOff)Convert.ToByte(ParamPreamp), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void SelectorButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(PreselectorSetting.SelectedIndex);
            string[] mas = AddValueFreqCombo();
            string Preselector = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPreselectorSettingCode5((SetPreampRange)Convert.ToByte(Preselector, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void FreqGain_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox1.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            string message = ""; string hex = "";
            message = LowAndHighByte.Text;
            int value = Convert.ToInt32(message);
            if (value < 30) { message = "30"; }
            else if (value > 6000) { message = "6000"; }
            value = Convert.ToInt32(message);
            hex = value.ToString("X");
            sendFreq = message;
            if (hex.Length != 0)
            {
                if (hex.Length == 2) { LowByteFreq = hex; HighByteFreq = "00"; }
                else if (hex.Length == 3)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 1);
                    HighByteFreq = $"0{str}";
                }
                else if (hex.Length == 4)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 2);
                    HighByteFreq = str;
                }
                int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
                string[] DeviceMas = AddValueDeviceAddressCombo();
                string DeviceAddress = DeviceMas[DeviceCount];
                p.SetFreqGainCode6((short)value, (byte)Convert.ToInt16(Gain, 16));
            }
            else { Display.AppendText($"Введите команду!\r\n"); }
        }

        private void SettingOperationMode_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(RangeBox.SelectedIndex);
            string[] mas = AddValueOperationModCombo();
            string Operation = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetOperationModCode7((ModePreamp)Convert.ToByte(Operation));
        }

        private void LinkButton_Click(object sender, RoutedEventArgs e)
        {
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetLinkCode8(Convert.ToByte(this.FirstNumberChannel.Text), Convert.ToByte(this.SecondNumberChannel.Text));
        }

        private void AttenuatorLevelButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox2.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            int count_1 = Convert.ToInt32(GainBox2_2.SelectedIndex);
            string[] mas_1 = AddValueGainCombo();
            string gain_1 = mas_1[count_1];
            p.SetAttenuatorLevelCode9_4b((byte)Convert.ToInt16(Gain, 16), (byte)Convert.ToInt16(gain_1, 16));
        }

        private void AttenuatorLevelLowRangeButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox3.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetAttenuatorLowLevelCodeA((byte)Convert.ToInt16(Gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void AttenuatorLevelHighRangeButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox4.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetAttenuatorHighLevelCodeB((byte)Convert.ToInt16(Gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void PowerButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(OnOffPreamp1.SelectedIndex);
            string[] mas = AddParamPreampCombo();
            string Power = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPowerCodeF((OnOffSwitch)Convert.ToByte(Power)); 
        }

        private void OpticalTranssmitterPower_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(OnOffPreamp2.SelectedIndex);
            string[] mas = AddParamPreampCombo();
            string Power = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetOpticalPowerCode10((OnOffSwitch)Convert.ToByte(Power));
        }

        private void SettingGainOperattionMode_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(RangeBox1.SelectedIndex);
            string[] mas = AddValueOperationModCombo();
            string Range = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetGainOperationModeCode11((ModePreamp)Convert.ToByte(Range));
        }

        private void SendSet_Click(object sender, RoutedEventArgs e)
        {
            string message = "";
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.AdditionalSettingsCode16();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            p.ResetCode14();
        }

        private void SaveButton1_Click(object sender, RoutedEventArgs e)
        {
            p.SetSaveCode13();
        }

        private string AddSpaces(string message)
        {
            int step = 2;
            string[] messageMass = message.Split(' ');
            message = string.Join("", messageMass);
            StringBuilder sb = new StringBuilder();
            sb.Append(message.Substring(0, step));
            for (int i = step; i < message.Length; i += step)
            {
                sb.Append(" ");
                for (int j = i; j < i + step && j < message.Length; j++)
                {
                    sb.Append(message[j]);
                }
            }
            message = sb.ToString();
            return message;
        }

        private void AddToCombo()
        {
            string[] mas = { "1", "2", "3", "4" };
            string[] mas_2 = { "выключить оба канала", "включить первый канал", "включить второй канал", "включить оба канала" };
            for (int k = 0; k < 4; k++)
            {
                //Box1.Items.Add(mas[k]);
                //Box2.Items.Add(mas[k]);
                //Box3.Items.Add(mas[k]);
                //Box4.Items.Add(mas[k]);
                //Box5.Items.Add(mas[k]);
                //Box6.Items.Add(mas[k]);
                //Box7.Items.Add(mas[k]);
                //Box8.Items.Add(mas[k]);
                FeedPreamp.Items.Add(mas_2[k]);
            }
            FeedPreamp.SelectedIndex = 0;
            PowerLaserBox.Items.Add("выключить");
            PowerLaserBox.Items.Add("включить");
            PowerLaserBox.SelectedIndex = 0;
            
        }

        private string[] AddValueFreqCombo()
        {
            string[] listValueFreq = new string[12];
            listValueFreq[0] = "00";
            listValueFreq[1] = "01";
            listValueFreq[2] = "02";
            listValueFreq[3] = "03";
            listValueFreq[4] = "04";
            listValueFreq[5] = "05";
            listValueFreq[6] = "06";
            listValueFreq[7] = "07";
            listValueFreq[8] = "08";
            listValueFreq[9] = "09";
            listValueFreq[10] = "0A";
            listValueFreq[11] = "0B";
            return listValueFreq;
        }

        private string[] AddRengFreqCombo()
        {
            string[] listRengFreq = new string[13];
            listRengFreq[0] = "30-70 МГц";
            listRengFreq[1] = "60-90 МГц";
            listRengFreq[2] = "75-131 МГц";
            listRengFreq[3] = "120-210 МГц";
            listRengFreq[4] = "186-340 МГц";
            listRengFreq[5] = "292-490 МГц";
            listRengFreq[6] = "470-880 МГц";
            listRengFreq[7] = "860-1500 МГц";
            listRengFreq[8] = "1500-3000 МГц";
            listRengFreq[9] = "3000-4000 МГц";
            listRengFreq[10] = "4000-5000 МГц";
            listRengFreq[11] = "5000-6000 МГц";
            listRengFreq[12] = "Wi-Fi 2.4 ГГц";
            return listRengFreq;
        }

        private string[] AddValueGainCombo()
        {
            string[] listGain = new string[32];
            listGain[0] = "00";
            listGain[1] = "01";
            listGain[2] = "02";
            listGain[3] = "03";
            listGain[4] = "04";
            listGain[5] = "05";
            listGain[6] = "06";
            listGain[7] = "07";
            listGain[8] = "08";
            listGain[9] = "09";
            listGain[10] = "0A";
            listGain[11] = "0B";
            listGain[12] = "0C";
            listGain[13] = "0D";
            listGain[14] = "0E";
            listGain[15] = "0F";
            listGain[16] = "10";
            listGain[17] = "11";
            listGain[18] = "12";
            listGain[19] = "13";
            listGain[20] = "14";
            listGain[21] = "15";
            listGain[22] = "16";
            listGain[23] = "17";
            listGain[24] = "18";
            listGain[25] = "19";
            listGain[26] = "1A";
            listGain[27] = "1B";
            listGain[28] = "1C";
            listGain[29] = "1D";
            listGain[30] = "1E";
            listGain[31] = "1F";
            return listGain;
        }

        private string[] AddGainCombo()
        {
            string[] listGain = new string[32];
            listGain[0] = "0 дБ";
            listGain[1] = "1 дБ";
            listGain[2] = "2 дБ";
            listGain[3] = "3 дБ";
            listGain[4] = "4 дБ";
            listGain[5] = "5 дБ";
            listGain[6] = "6 дБ";
            listGain[7] = "7 дБ";
            listGain[8] = "8 дБ";
            listGain[9] = "9 дБ";
            listGain[10] = "10 дБ";
            listGain[11] = "11 дБ";
            listGain[12] = "12 дБ";
            listGain[13] = "13 дБ";
            listGain[14] = "14 дБ";
            listGain[15] = "15 дБ";
            listGain[16] = "16 дБ";
            listGain[17] = "17 дБ";
            listGain[18] = "18 дБ";
            listGain[19] = "19 дБ";
            listGain[20] = "20 дБ";
            listGain[21] = "21 дБ";
            listGain[22] = "22 дБ";
            listGain[23] = "23 дБ";
            listGain[24] = "24 дБ";
            listGain[25] = "25 дБ";
            listGain[26] = "26 дБ";
            listGain[27] = "27 дБ";
            listGain[28] = "28 дБ";
            listGain[29] = "29 дБ";
            listGain[30] = "30 дБ";
            listGain[31] = "31 дБ";
            return listGain;
        }

        private string[] AddValueDeviceAddressCombo()
        {
            string[] listValueDevice = new string[3];
            listValueDevice[0] = "00";
            listValueDevice[1] = "01";
            listValueDevice[2] = "02";
            return listValueDevice;
        }

        private string[] AddDeviceAddressCombo()
        {
            string[] listDevice = new string[3];
            listDevice[0] = "Синхронный";
            listDevice[1] = "1-ый модуль";
            listDevice[2] = "2-ой модуль";
            return listDevice;
        }

        private string[] AddParamPreampCombo()
        {
            string[] listParamPreamp = new string[2];
            listParamPreamp[0] = "00";
            listParamPreamp[1] = "01";
            return listParamPreamp;
        }

        private string[] AddParamPreampCombo1()
        {
            string[] listParamPreamp = new string[3];
            listParamPreamp[0] = "00";
            listParamPreamp[1] = "01";
            listParamPreamp[2] = "02";
            return listParamPreamp;
        }

        private string[] AddPreampCombo()
        {
            string[] listParamPreamp = new string[2];
            listParamPreamp[0] = "Для всех диапозонов";
            listParamPreamp[1] = "Для текущего диапозона";
            return listParamPreamp;
        }

        private string[] OnOffPreampCombo()
        {
            string[] listPreamp = new string[2];
            listPreamp[0] = "Выключить";
            listPreamp[1] = "Включить";
            return listPreamp;
        }

        private string[] OnOffPreampCombo1()
        {
            string[] listPreamp = new string[2];
            listPreamp[0] = "Включить";
            listPreamp[1] = "Выключить";
            return listPreamp;
        }

        private string[] AddValueOperationModCombo()
        {
            string[] listValueOperationMode = new string[2];
            listValueOperationMode[0] = "00";
            listValueOperationMode[1] = "01";
            return listValueOperationMode;
        }

        private string[] AddOperationModCombo()
        {
            string[] listOperationMode = new string[2];
            listOperationMode[0] = "Для всех";
            listOperationMode[1] = "Для каждого";
            return listOperationMode;
        }

        private string[] AddValueLinkCombo()
        {
            string[] listValueLink = new string[5];
            listValueLink[0] = "00";
            listValueLink[1] = "01";
            listValueLink[2] = "02";
            listValueLink[3] = "03";
            listValueLink[4] = "04";
            return listValueLink;
        }

        private string[] AddLinkCombo()
        {
            string[] listLink = new string[5];
            listLink[0] = "Для всех выключен";
            listLink[1] = "Для 1-ого РЧ-канала";
            listLink[2] = "Для 2-ого РЧ-канала";
            listLink[3] = "Для 3-ого РЧ-канала";
            listLink[4] = "Для 4-ого РЧ-канала";
            return listLink;
        }

        private void LowAndHighByte_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            string text = tb.Text.ToUpper();
            char[] symbols = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            tb.Text = new string(text.Where(x => symbols.Contains(x)).ToArray());
            LowAndHighByte.SelectionStart = LowAndHighByte.Text.Length;
        }

        private void FeedPreampButton_Click(object sender, RoutedEventArgs e)
        {
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.FeedChannelCodeE((FeedChannels)((byte)FeedPreamp.SelectedIndex));
        }

        private void SetTract_Click(object sender, RoutedEventArgs e)
        {
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPathGainCode15((byte)Convert.ToInt16(TracktBox.Text));
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            p.AdditionalSettingsCode16();
        }

        private void PowerLaserButton_Click(object sender, RoutedEventArgs e)
        {
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetLaserPowerCode17((OnOffSwitch)Convert.ToByte(PowerLaserBox.SelectedIndex));
        }

        private void SendGain_2_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox2_3.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetGainCode2_3b((byte)Convert.ToInt16(gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void AttenuatorLevelButton_2_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox2_1.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetAttenuatorLevelCode9_3b((byte)Convert.ToInt16(Gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void _18Button_Click(object sender, RoutedEventArgs e)
        {
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            this.p.RFAttenuatorControlCode18(Convert.ToByte(this._18Gain.Text), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void _19Button_Click(object sender, RoutedEventArgs e)
        {
            this.p.QueryCurrentStateCode19();
        }

        private void _1AButton_Click(object sender, RoutedEventArgs e)
        {
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            this.p.ResetSetupCode1A((PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void _1BButton_Click(object sender, RoutedEventArgs e)
        {
            this.p.RequestingPatchCodesCode1B();
        }

        private void ConnectTCPButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.preselTcp = new PreselTcp(IpBox.Text, Convert.ToInt16(PortBox.Text));
                this.p = this.preselTcp as PreselModel;
                var a = p.Connect();
                if (a == true)
                {
                    CheckEl.Fill = new SolidColorBrush(Colors.PaleGreen);
                    this.preselTcp.OnRead += Client_DataReceived;
                    this.preselTcp.OnWrite += P_OnWriteBTcp;
                    p.OnPreselectorNumber += P_OnPreselectorNumber;
                    p.OnState_00 += P_OnState;
                    p.OnFreq_01 += P_OnFreq01;
                    p.OnGain_02_3b += P_OnGain02_3b;
                    p.OnGain_02_4b += P_OnGain02_4b;
                    p.OnModePreamp_03 += P_OnModePreamp;
                    p.OnOnOffPreamp_04 += P_OnOnOffPreamp;
                    p.OnSetPreampRange_05 += P_OnSetPreampRange;
                    p.OnSetFreqGain_06 += P_OnSetFreqGain_06;
                    p.OnModeExternalSwitches_07 += P_OnModeExternalSwitches;
                    p.OnManagementExternalSwitches_08 += P_OnRFChannel;
                    p.OnGain_09_3b += P_OnGain09_3b;
                    p.OnGain_09_4b += P_OnGain09_4b;
                    p.OnAttenuatorLevelLow_0A += P_OnAttenuatorLevelLow;
                    p.OnAttenuatorLevelHigh_0B += P_OnAttenuatorLevelHigh;
                    p.OnFeedChannel_0E += P_OnFeedChannel;
                    p.OnOnOffSwitch_0F += P_OnOnOffSwitch;
                    p.OnOnOffOpticalTransmitter_10 += P_OnOnOffOpticalTransmitter;
                    p.OnModeAttenuator_11 += P_OnModeAttenuator;
                    p.OnConfigurationChanels_12 += P_OnConfigurationChanels;
                    p.OnSave_13 += P_OnSave;
                    p.OnReset_14 += P_OnReset;
                    p.OnGain_15 += P_OnGain15;
                    p.OnAdditionalState_16 += P_OnAdditionalState;
                    p.OnFeedLaser_17 += P_OnFeedLaser;
                    this.p.OnGainRFAttenuator_18 += P_OnGainRFAttenuator_18;
                    this.p.OnCurrentStatusOfTuners_19_4b += P_OnCurrentStatusOfTuners_19_4b;
                    this.p.OnCurrentStatusOfTuners_19_6b += P_OnCurrentStatusOfTuners_19_6b;
                    this.p.OnReset_1A += P_OnReset_1A;
                    this.p.OnSwitchingCodes_1B += P_OnSwitchingCodes_1B;
                }
            }
            catch
            {

            }
        }

        private void P_OnWriteBTcp(object sender, string e)
        {
            try
            {
                string b = string.Empty;
                foreach (var a in e)
                {
                    b += a.ToString();
                }
                b = AddSpaces(b);
                this.Dispatcher.Invoke(() => Display.AppendText(b + "\r\n"));
            }
            catch { }
        }

        private void Client_DataReceived(object sender, string e)
        {
            try
            {
                string b = string.Empty;
                //foreach (var a in e)
                //{
                //    b += a.ToString();
                //}
                b = AddSpaces(e);
                this.Dispatcher.Invoke(() => Display.AppendText("Answer: " + b + "\r\n"));
            }
            catch { }
        }

        private void TypeConnectionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            typeConnection = Convert.ToBoolean(TypeConnectionComboBox.SelectedIndex);
        }

        private void DisconnectTcpButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                p.Disconnect();
                CheckEl.Fill = new SolidColorBrush(Colors.Red);
            }
            catch
            {

            }
        }
    }
}
